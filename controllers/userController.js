const express = require('express');
const router = new express.Router();
const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');
const {asyncWrapper} = require('../utils/apiUtils');


router.get(
    '/me',
    asyncWrapper(async (req, res) => {
      const userId = req.user.userId;

      const userInfo = await User.findById(userId);

      const userDto = {
        user: {
          _id: userInfo._id,
          role: userInfo.role,
          email: userInfo.email,
          created_date: userInfo.created_date,
        },
      };
      res.json(userDto);
    }),
);

router.delete(
    '/me',
    asyncWrapper(async (req, res) => {
      const userId = req.user.userId;

      await User.findByIdAndRemove(userId);
      res.json('Profile deleted successfully');
    }),
);

router.patch(
    '/me',
    asyncWrapper(async (req, res) => {
      const userId = req.user.userId;

      const {oldPassword, newPassword} = req.body;

      const user = await User.findById(userId);

      const hashEquals = await bcrypt.compare(oldPassword, user.passwordHash);

      if (!hashEquals) {
        throw new Error('Invalid old password');
      }

      const newHash = await bcrypt.hash(newPassword, 10);

      await User.updateOne({_id: userId}, {passwordHash: newHash});

      res.json('Password changed successfully');
    }),
);

module.exports = {
  userController: router,
};
