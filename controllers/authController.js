const express = require('express');
const router = new express.Router();

const {registration, login} = require('../services/authService');

const {asyncWrapper} = require('../utils/apiUtils');
const {registrationValidator} = require('../middlewares/authValidator');

router.post(
    '/register',
    registrationValidator,
    asyncWrapper(async (req, res) => {
      const {email, password, role} = req.body;

      await registration({email, password, role});

      res.json({message: 'Profile created successfully'});
    }),
);

router.post(
    '/login',
    asyncWrapper(async (req, res) => {
      const {email, password} = req.body;

      const token = await login({email, password});

      res.json({jwt_token: token});
    }),
);

module.exports = {
  authController: router,
};
