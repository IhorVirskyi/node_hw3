const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../utils/apiUtils');
const {addTruck} = require('../services/truckService');
const {Truck} = require('../models/truckModel');
const {truckValidator} = require('../middlewares/truckValidator');
const {driverOnlyMiddleware} = require('../middlewares/routeGuards');

// add only for driver!!
router.post(
    '/',
    [driverOnlyMiddleware, truckValidator],
    asyncWrapper(async (req, res) => {
      const {type} = req.body;
      await addTruck({type, userId: req.user.userId});
      res.json({message: 'Truck created successfully'});
    }),
);

router.get(
    '/',
    [driverOnlyMiddleware],
    asyncWrapper(async (req, res) => {
      const trucks = await Truck
          .find({created_by: req.user.userId}).select('-__v');
      res.json({trucks});
    }),
);

router.get(
    '/:id',
    [driverOnlyMiddleware],
    asyncWrapper(async (req, res) => {
      const truckId = req.params.id;

      const userId = req.user.userId;
      const truckInfo = await Truck
          .findOne({_id: truckId, created_by: userId}).select('-__v');

      if (!truckInfo) {
        return res.status(400).json({message: 'no truck'});
      }
      const truck = {
        _id: truckInfo._id,
        created_by: truckInfo.created_by,
        assigned_to: truckInfo.assigned_to,
        type: truckInfo.type,
        status: truckInfo.status,
        created_date: truckInfo.created_date,
      };

      res.json({truck});
    }),
);

router.put(
    '/:id',
    [driverOnlyMiddleware],
    asyncWrapper(async (req, res) => {
      const truckId = req.params.id;
      const {type} = req.body;

      const truckInfo = await Truck.
          findOne({_id: truckId, created_by: req.user.userId});

      if (truckInfo.status !== 'IS') {
        return res.status(400).json(
            {
              message: 'You cannot change type of truck while you are on drive',
            },
        );
      }

      truckInfo.type = type;
      await truckInfo.save();
      res.json({message: 'Truck details changed successfully'});
    }),
);

router.delete(
    '/:id',
    [driverOnlyMiddleware],
    asyncWrapper(async (req, res) => {
      const truckId = req.params.id;

      await Truck.findOneAndRemove({_id: truckId});
      res.json({message: 'Truck deleted successfully'});
    }),
);

router.post(
    '/:id/assign',
    [driverOnlyMiddleware],
    asyncWrapper(async (req, res) => {
      const truckId = req.params.id;

      const truckInfo = await Truck.findById(truckId);

      const assignedTruck = await Truck
          .findOne({assigned_to: req.user.userId, created_by: req.user.userId});

      if (assignedTruck) {
        if (assignedTruck.status !== 'IS') {
          res.status(400).json(
              {message: 'You are already assigned to the truck'});
        } else {
          assignedTruck.assigned_to = null;
          truckInfo.assigned_to = req.user.userId;

          await assignedTruck.save();
          await truckInfo.save();


          return res.json({message: 'Truck assigned successfully'});
        }
      }

      truckInfo.assigned_to = req.user.userId;
      await truckInfo.save();
      return res.json({message: 'Truck assigned successfully'});
    }),
);


module.exports = {
  truckController: router,
};
