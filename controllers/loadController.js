const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('../utils/apiUtils');
const {addLoad} = require('../services/loadService');
const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');
const {loadValidator} = require('../middlewares/loadValidator');
const {shipperOnlyMiddleware} = require('../middlewares/routeGuards');
const {driverOnlyMiddleware} = require('../middlewares/routeGuards');

// TODO: Only for SHipper
router.post(
    '/',
    [shipperOnlyMiddleware, loadValidator],
    asyncWrapper(async (req, res) => {
      const loadBody = req.body;
      await addLoad({
        created_by: req.user.userId,
        assigned_to: null,
        status: 'NEW',
        state: null,
        ...loadBody,
      });
      res.json({message: 'Load created successfully'});
    }),
);

// TODO: add logic for driver and shit
router.get(
    '/',
    asyncWrapper(async (req, res) => {
      const getDefaultValue = (num, min, max) => {
        if (!num) return min;
        return num <= min ?
            min :
            num >= max ?
              max :
              num;
      };
      const limit = getDefaultValue(parseInt(req.params.limit), 10, 50);
      const offset = getDefaultValue(
          parseInt(req.params.offset), 0, Number.MAX_SAFE_INTEGER);
      // const status = req.query.status;
      if (req.user.role === 'SHIPPER') {
        const loads = await Load
            .find({created_by: req.user.userId})
            .skip(offset)
            .limit(limit)
            // .where('status').equals(status)
            .select('-__v');
        res.json({loads});
      } else {
        const loads = await Load
            .find({status: {$in: ['ASSIGNED', 'SHIPPED']}})
            .skip(offset)
            .limit(limit)
            // .where('status').equals(status)
            .select('-__v');
        res.json({loads});
      }
    }),
);

// TODO: only for driver
router.get(
    '/active',
    [driverOnlyMiddleware],
    asyncWrapper(async (req, res) => {
      const load = await Load.findOne({
        status: 'ASSIGNED',
        assigned_to: req.user.userId}).select('-__v');
      res.json({load});
    }),
);

// TODO: all
router.patch(
    '/active/state',
    [driverOnlyMiddleware],
    asyncWrapper(async (req, res) => {
      const loadInfo = await Load.
          findOne({assigned_to: req.user.userId, status: 'ASSIGNED'});
      if (!loadInfo) {
        return res.status(404).json({message: 'no assigned loads'});
      }
      switch (loadInfo.state) {
        case 'En route to Pick Up':
          loadInfo.state = 'Arrived to Pick Up';
          break;
        case 'Arrived to Pick Up':
          loadInfo.state = 'En route to delivery';
          break;
        case 'En route to delivery':
          const truckInfo = await Truck.findOne({assigned_to: req.user.userId});
          truckInfo.status = 'IS';
          await truckInfo.save();

          loadInfo.state = 'Arrived to delivery';
          loadInfo.status = 'SHIPPED';
          break;
        case 'Arrived to delivery': {
          return res.
              json({message: `Load's already delivered`});
        }
      }
      await loadInfo.save();
      res.json({message: `Load state changed to ${loadInfo.state}`});
    }),
);

// do i need created_by here?
router.get(
    '/:id',
    asyncWrapper(async (req, res) => {
      const loadId = req.params.id;
      const load = await Load
          .findOne({_id: loadId, created_by: req.user.userId}).select('-__v');
      if (!load) {
        return res.status(400).json({message: 'no such load'});
      }
      res.json({load});
    }),
);

// TODO: Check all
router.put(
    '/:id',
    [shipperOnlyMiddleware],
    asyncWrapper(async (req, res) => {
      const loadId = req.params.id;
      const loadBody = req.body;
      const loadInfo = await Load.
          findOne({_id: loadId, created_by: req.user.userId});
      if (!loadInfo) {
        return res.status(400)
            .json({message: 'need to create load'});
      }
      if (loadInfo.status !== 'ASSIGNED') {
        await loadInfo.update(loadBody);
        res.json({message: 'Load details changed successfully'});
      } else {
        res.status(400).
            json({message: 'load is already assigned and with driver'});
      }
    }),
);

// check load status if new
router.delete(
    '/:id',
    [shipperOnlyMiddleware],
    asyncWrapper(async (req, res) => {
      const load = Load.findOne({_id: loadId, created_by: req.user.userId});
      if (!load) {
        return res.status(400).
            json({message: 'no load in bd'});
      }
      if (load.status === 'NEW') {
        await load.remove();
        res.json({message: 'Load deleted successfully'});
      } else {
        res.status(400).
            json({message: 'load is already assigned and with driver'});
      }
    }),
);

// TODO: add POST and GET

router.get(
    '/:id/shipping_info',
    [shipperOnlyMiddleware],
    asyncWrapper(async (req, res) => {
      const userId = req.user.userId;
      const loadId = req.params.id;
      const load = await Load.
          findOne({_id: loadId, created_by: userId}).select('-__v');
      if (!load) {
        return res.status(400).json({message: 'no such load'});
      }

      // TODO: WRONG LOGIC
      const assignedTo = load.assigned_to;

      if (assignedTo === null) {
        return res.json({load}).select('-__v');
      }

      const truck = await Truck
          .findOne({assigned_to: assignedTo}).select('-__v');

      res.json({load, truck});
    }),
);

// TODO: change status to posted
router.post(
    '/:id/post',
    [shipperOnlyMiddleware],
    asyncWrapper(async (req, res) => {
      const loadId = req.params.id;
      const userId = req.user.userId;
      const load = await Load.
          findOne({_id: loadId, created_by: userId,
            status: 'NEW',
          });
      if (!load) {
        return res.status(400).json({message: 'no such load'});
      }
      const trucksDimensions = {
        'SPRINTER': {
          width: 300,
          length: 250,
          height: 170,
          payload: 1700,
        },
        'SMALL STRAIGHT': {
          width: 500,
          length: 250,
          height: 170,
          payload: 2500,
        },
        'LARGE STRAIGHT': {
          width: 700,
          length: 350,
          height: 200,
          payload: 4000,
        },
      };

      const trucks = await Truck.find(
          {
            assigned_to: {$ne: null},
            status: 'IS',
          },
      );
      const filteredTrucks = trucks.filter((x) => {
        const dim = trucksDimensions[x.type];
        return dim.width > load.dimensions.width &&
               dim.height > load.dimensions.height &&
               dim.length > load.dimensions.length &&
               dim.payload > load.payload;
      });

      const foundTruck = filteredTrucks[0];

      if (!foundTruck) {
        return res.status(404).json({
          message: 'Load posted unsuccessfully',
          driver_found: false,
        });
      }

      foundTruck.status = 'OS';
      load.status = 'ASSIGNED';
      load.state = 'En route to Pick Up';
      load.assigned_to = foundTruck.assigned_to;

      await foundTruck.save();
      await load.save();
      return res.json({
        message: 'Load posted successfully',
        driver_found: true,
      });
    }),
);


module.exports = {
  loadController: router,
};
