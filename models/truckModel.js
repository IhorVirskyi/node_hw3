const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  created_by: {
    type: String,
    required: true,
  },

  assigned_to: {
    type: String,
  },

  type: {
    type: String,
    required: true,
  },

  status: {
    type: String,
    required: true,
  },

  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {Truck};
