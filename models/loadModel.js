const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },

  assigned_to: {
    type: String,
    optional: true,
  },

  status: {
    type: String,
  },

  state: {
    type: String,
  },

  name: {
    type: String,
    required: true,
  },

  payload: {
    type: Number,
    required: true,
  },

  pickup_address: {
    type: String,
    required: true,
  },

  delivery_address: {
    type: String,
    required: true,
  },

  dimensions: {
    type: Object,
    required: true,
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: {
    type: Array,
    required: true,
    message: {
      type: String,
      required: true, // /??
    },
    time: {
      type: Date,
      required: true,
    },
  },

  created_date: {
    type: Date,
    default: Date.now(),
  },
});

schema.pre('save', function(next) {
  // do stuff
  if (this.isModified('state')) {
    this.logs.push({
      message: `Changed state to ${this.state}`,
      time: new Date().toISOString(),
    });
  }

  if (this.isModified('status')) {
    this.logs.push({
      message: `Changed status to ${this.status}`,
      time: new Date().toISOString(),
    });
  }

  next();
});

const Load = mongoose.model('Load', schema);

module.exports = {Load};
