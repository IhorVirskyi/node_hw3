const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const app = express();

const {authController} = require('./controllers/authController');
const {NodeCourseError} = require('./utils/errors');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {userController} = require('./controllers/userController');
const {truckController} = require('./controllers/truckController');
const {loadController} = require('./controllers/loadController');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authController);
app.use('/api/users', [authMiddleware], userController);
app.use('/api/trucks', [authMiddleware], truckController);
app.use('/api/loads', [authMiddleware], loadController);

app.use((err, req, res, next) => {
  if (err instanceof NodeCourseError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});


const start = async () => {
  try {
    await mongoose.connect(
        'mongodb+srv://Ihor:EPAM@cluster0.meje8.mongodb.net/hw2?retryWrites=true&w=majority',
        {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useCreateIndex: true,
        },
    );

    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
