const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');

const registration = async ({email, password, role}) => {
  const user = new User({
    email: email,
    passwordHash: await bcrypt.hash(password, 10),
    role: role,
  });
  await user.save();
};

const login = async ({email, password}) => {
  const user = await User.findOne({email});

  if (!user) {
    throw new Error('Invalid username or password');
  }

  if (!(await bcrypt.compare(password, user.passwordHash))) {
    throw new Error('Invalid username or password');
  }

  const token = jwt.sign(
      {
        _id: user._id,
        email: user.email,
        role: user.role,
      },
      'secret',
  );
  return token;
};

module.exports = {
  registration,
  login,
};
