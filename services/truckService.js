const {Truck} = require('../models/truckModel');

const addTruck = async ({type, userId}) => {
  // const trucksDimensions = {
  //   'SPRINTER': {
  //     weight: 300,
  //     length: 250,
  //     height: 170,
  //     payload: 1700,
  //   },
  //   'SMALL STRAIGHT': {
  //     weight: 500,
  //     length: 250,
  //     height: 170,
  //     payload: 2500,
  //   },
  //   'LARGE STRAIGHT': {
  //     weight: 700,
  //     length: 350,
  //     height: 200,
  //     payload: 4000,
  //   },
  // };

  const truck = new Truck({
    created_by: userId,
    assigned_to: null,
    type,
    status: 'IS',
    // dimensions: trucksDimensions[type],
    // payload: trucksDimensions[type].payload,
  });

  const savedTruck = await truck.save();

  return savedTruck;
};

module.exports = {
  addTruck,
};
