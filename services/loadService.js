const {Load} = require('../models/loadModel');

const addLoad = async (loadBody) => {
  const load = new Load(loadBody);

  const savedLoad = await load.save();

  return savedLoad;
};

module.exports = {
  addLoad,
};
