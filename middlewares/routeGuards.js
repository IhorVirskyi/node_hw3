const shipperOnlyMiddleware = (req, res, next) => {
  const role = req.user.role;
  if (role === 'SHIPPER') {
    next();
  } else {
    return res.status(403).json({message: 'forbidden endpoint'});
  }
};

const driverOnlyMiddleware = (req, res, next) => {
  const role = req.user.role;
  if (role === 'DRIVER') {
    next();
  } else {
    return res.status(403).json({message: 'forbidden endpoint'});
  }
};

module.exports = {
  shipperOnlyMiddleware,
  driverOnlyMiddleware,
};

// add midleware + delete if..
// check delete load
// check  update
