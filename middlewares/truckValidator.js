
const Joi = require('joi');

const truckValidator = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi
        .string()
        .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
        .required(),
  });
  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};


module.exports = {
  truckValidator,
};
