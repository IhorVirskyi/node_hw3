
const Joi = require('joi');

const loadValidator = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string().required(),
    payload: Joi.number().required(),
    pickup_address: Joi.string().required(),
    delivery_address: Joi.string().required(),
    dimensions: Joi.object({
      width: Joi.number().required(),
      length: Joi.number().required(),
      height: Joi.number().required(),
    }),
  });
  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};


module.exports = {
  loadValidator,
};
